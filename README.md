# CS-E4000: Seminar in Computer Science 2020

Tutor: Mario Di Francesco

Topic: Solutions for CI/CD: a comparison study

> Most widely used hosted versioning systems support built-in and (or) third-party solutions for continuous integration and delivery (deployment). But how is this accomplished? What are the specific features of major cloud-based services? How to choose between them? The focus of this seminar topic is to introduce the concepts of continuous integration and delivery (deployment), then review and compare the features of the prevalent hosted versioning systems: BitBucket Pipelines, GitHub Actions, GitLab CI/CD, CircleCI and Travis CI.
> 
> References:
> 
>    https://bitbucket.org/blog/an-introduction-to-bitbucket-pipelines
>
>    https://help.github.com/en/actions/automating-your-workflow-with-github-actions/about-github-actions
>
>    https://docs.gitlab.com/ee/ci/
>
>    https://circleci.com/docs/
>
>    https://docs.travis-ci.com/user/tutorial/

Key Dates:

* 30.01.2020: First draft
* 02.03.2020: Second draft
* 09.03.2020: Opponent feedback
* 13.04.2020: Final paper
* 23.04.2020: Final slides
* 24.04.2020: Seminar Day

All submission deadlines at 23:59 local time in Finland.

## License

Unless otherwise noted, the entire contents of the repository (in particular the seminar paper and presentation) are licensed under Creative Commons Attribution 4.0 Internal License (CC BY 4.0).

https://creativecommons.org/licenses/by/4.0/
