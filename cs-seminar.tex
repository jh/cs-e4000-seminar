\documentclass[article]{aaltoseries}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage{rotating}
\usepackage{tabularx}
\usepackage{geometry}
\usepackage{listings}
\usepackage{setspace}
\usepackage{fontawesome}
\usepackage[hyperfootnotes=false]{hyperref}
\def\UrlBreaks{\do\/\do-}
\newcommand\todo[1]{\textcolor{red}{TODO: #1}}

% define listing style for yaml code
% from https://tex.stackexchange.com/a/152856
\newcommand\YAMLcolonstyle{\color{red}\mdseries}
\newcommand\YAMLkeystyle{\color{black}\bfseries\ttfamily\footnotesize}
\newcommand\YAMLvaluestyle{\color{blue}\mdseries}
\makeatletter
% here is a macro expanding to the name of the language
% (handy if you decide to change it further down the road)
\newcommand\language@yaml{yaml}
\expandafter\expandafter\expandafter\lstdefinelanguage
\expandafter{\language@yaml}
{
  keywords={true,false,null,y,n},
  keywordstyle=\color{darkgray}\bfseries,
  basicstyle=\YAMLkeystyle,
  sensitive=false,
  showspaces=false,
  showtabs=false,
  showstringspaces=false,
  comment=[l]{\#},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\YAMLvaluestyle\ttfamily,
  moredelim=[l][\color{orange}]{\&},
  moredelim=[l][\color{magenta}]{*},
  moredelim=**[il][\YAMLcolonstyle{:}\YAMLvaluestyle]{:},
  morestring=[b]',
  morestring=[b]",
  literate =    {---}{{\ProcessThreeDashes}}3
                {>}{{\textcolor{red}\textgreater}}1
                {|}{{\textcolor{red}\textbar}}1
                {\ -\ }{{\mdseries\ -\ }}3,
}
% switch to key style at EOL
\lst@AddToHook{EveryLine}{\ifx\lst@language\language@yaml\YAMLkeystyle\fi}
\makeatother
\newcommand\ProcessThreeDashes{\llap{\color{cyan}\mdseries-{-}-}}

% define table columns for left-/right-alignment
% from https://tex.stackexchange.com/a/97188
\usepackage{array}
\newcolumntype{L}{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}X}
\newcolumntype{R}{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}X}

\begin{document}

%=========================================================

\title{A comparison study of managed CI/CD solutions}

\author{Jack Henschel% Your first and last name: do _not_ add your student number
\\\textnormal{\texttt{jack.henschel@aalto.fi}}} % Your Aalto e-mail address

\affiliation{\textbf{Tutor}: Mario Di Francesco} % First and last name of your tutor

\maketitle

%==========================================================

\begin{abstract}
  Continuous practices are widely used in software development, from hobby programmers to small startups and large enterprises.
  These practices are facilitated by a chain of tools, which provides software engineers with insights from code review, build, unit and integration test, package, to staging and production deployment.
  The key factor is the automation of every single step.
  Presently, version control has mostly converged on a few well-known tools, while the landscape of CI/CD systems is vast and rapidly evolving.
  This paper outlines the benefits of continuous integration, delivery and deployment practices. Specifically, it examines the shared as well as specific features of prevalent general-purpose CI/CD systems: AppVeyor, Bitbucket Pipelines, CircleCI, Cirrus CI, Concourse CI, Drone, GitHub Actions, GitLab CI/CD and Travis CI.
  Additional investigation is performed on why developers should and how they can utilize these features, thus providing guidance on choosing an appropriate solution.
  \vspace{3mm}
  \\
\noindent KEYWORDS: continuous, integration, deployment, delivery, tools

\end{abstract}


%============================================================


\section{Introduction}
\label{sec:introduction}

The speed of software development has rapidly increased over the last decades.
Developers are writing more advanced software and build complex systems out of many individual components.
New software engineering practices have emerged to support this increased development speed.
Presently, continuous practices are widely used in software development, from hobby projects to small startups and large enterprises.
They are rooted in agile software development methodologies \cite{matthies_attitudes_2019} and comprise three stages: continuous integration, continuous delivery and continuous deployment.

Continuous integration (CI) started to be used in 2000: every small change to the source code of a software is evaluated for compliance with a test suite and merged into the main development branch of the project \cite{hilton_usage_2016}.
These tests can be unit tests that individually test small pieces of the code, or integration tests that ensure the software components work together as expected.
There may also be performance tests to assess responsiveness (changes) in the software, or end-to-end tests which take the software as a whole and simulate user interactions \cite{atlassian_different_nodate}.
While the tests can be run locally, it is beneficial to run them on a single source of truth: a shared build service.
Since all these testing steps can be automatically performed with each small change in the software (\textit{commit}), the software developer team can be confident that the application still works as intended.

Continuous delivery (CDE) is the next step in the evolution of automating software engineering: after verifying the software works as expected, a package -- such as a tarball, container image or binary -- is automatically created and published to an artifact repository.
%% The ultimate goal of CDE is to ``keep producing valuable software in short cycles and ensure that the software can be reliably released at any time''.
According to Chen~\cite{chen_continuous_2015}, the ultimate goal of CDE is reliably producing software which can be released and deployed in short cycles.
Thus, benefits of CDE strategies include an accelerated time to market of features, improved productivity of developers and more reliable releases.

Continuous deployment (CD) is the final step: the software packages are also automatically installed and provisioned on the target systems, which is only possible if these are also controlled by the same organization (Figure \ref{fig:ci-cd-workflow}).
It may seem like a small, logical step, but in practice it has major implications for the entire infrastructure of an organization:
all systems need to be designed for quick and fully automatic provisioning of new versions of the software, while also providing the possibility of rolling back and avoiding any inconsistent state.
The ability to roll back a failed deployment is critical for CD since it reduces the \textit{mean time to recovery} and minimizes the impact of outages or degraded performance.
This final step is closely related to the concept of DevOps, which is an ``organizational effort to automate continuous delivery of new software updates while guaranteeing their correctness and reliability'' \cite{leite_survey_2020}.

A release process entirely described through configuration and code becomes less intimidating than a manual one, simply because releases happen much more frequently.
Therefore, flaws in the release process are discovered faster and, most importantly, they are fixed before the next release since the code is patched. % fixed
This paradigm is also reflected in the principles of agile \cite{noauthor_principles_2001}.
Individual \textit{tasks}, also called \textit{steps} or \textit{jobs}, are combined into more complex \textit{pipelines}, also referred to as \textit{workflows}.

Online platforms for sharing source code exist since the advent of the web, while build services are a more recent development.
%% One of the first major applications was Hudson, later called Jenkins, an open-source tool for testing, building and deploying software \cite{hilton_usage_2016}.
When Travis~CI launched in 2011, it was one of the first easy-to-use CI/CD services freely available to anyone \cite{vasilescu_continuous_2014}.
At present, there are over 50 different CI/CD platforms with a wide range of features and use cases \cite{bronnikov_ligurioawesome-ci_2020}.

The goal of this paper is to help practitioners choose an appropriate platform for their environment and provide guidance on how to use its features.
In particular, common and specific features of nine platforms are examined: AppVeyor, Bitbucket Pipelines, CircleCI, Cirrus CI, Concourse CI, Drone, GitHub Actions, GitLab CI/CD and Travis CI.

This paper is organized as follows.
Section \ref{sec:ci-cd-process} presents the key factors and steps of the CI/CD process.
Section \ref{sec:feature-comparison} outlines important features for this process and compares their availability in popular products.
Finally, Section \ref{sec:conclusion} provides concluding remarks and outlines future work.

%============================================================
\section{CI/CD process}
\label{sec:ci-cd-process}

%% As Savor et al. noted \cite{savor_continuous_2016}, the ``key elements of continuous deployment are:
%% 1. software updates are kept as small and isolated as reasonably feasible;
%% 2. they are released for deployment immediately after development and testing completes;
%% 3. the decision to deploy is largely left up to the developers (without the use of separate testing teams); and
%% 4. deployment is fully automated''.

As Savor \textit{et al.}~\cite{savor_continuous_2016} noted, continuous deployment consists of four key elements: small software updates, automatic releases, responsibility on the developers, and fully automatic deployment.
Continuous deployment is an approach in which a team of developers is responsible for their code during the entire software lifecycle.
Tools help manage this responsibility by providing insights into code changes, automating common tasks to make them repeatable and error free, and logging every action of the process \cite{savor_continuous_2016}.

%% Commit & Push - Build - Test(Lint - Unit Test - Integration Test - Performance Test) - Code Review - Deployment(Deploy to Staging - Promote to Production) - Monitoring
%% ............... ==================================================================================...=====================================================
%%                                         CI                                                                    CD

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=\textwidth]{figures/ci-cd-workflow}
    \caption{Continuous integration and continuous deployment workflow.}
    \label{fig:ci-cd-workflow}
  \end{center}
\end{figure}

The entire CI/CD pipeline is depicted in Figure \ref{fig:ci-cd-workflow} and consists of two phases (CI and CD), which comprise multiple steps each.
Depending on the application and organization, every single step may not be necessary, but each one helps reduce the risk of a failure in production.
After the developers have written their code, they upload, i.e., \textit{push}, it to a shared code repository.

At the beginning of the first phase, the CI system notices the change and automatically triggers the testing pipeline.
This pipeline runs the unit, integration, and performance tests outlined in Section \ref{sec:introduction} and notifies the initial developer in case of a failure.
Afterwards and optionally, another developer is prompted about the changes to review them.
The human review is useful to identify non-functional problems in the code, such as architectural and design decisions.

In the second phase, the CD system runs the delivery and deployment steps.
If necessary, the software is packaged into a deliverable format, such as an installer, archive or distribution package.
Next, this artifact is copied onto the target system, usually a staging environment, which mirrors the production environment as closely as possible.
If the software is observed to run fine there, then a \textit{promotion} to the production environment can happen either automatically or manually through a human in a web interface.
The final step, which not all CI/CD systems implement, is monitoring of the running application, including performance metrics and logs.
If any failures occur, the application is rolled back to the previous working version in production, depending on the deployment strategy \cite{savor_continuous_2016}.
Some CI/CD systems, such as Bitbucket Pipelines~\footnote{\url{https://confluence.atlassian.com/bitbucket/bitbucket-deployments-940695276.html}}, natively support this distinction between different deployment environments.
%This enhances the developers' visibility into which version of the software is currently running in each environment.

While in principle the CI and CD systems may be separate, they are often combined into a single application~\footnote{\url{https://about.gitlab.com/handbook/product/single-application/}} to leverage similarities and enhance integration between the two phases.

%============================================================
\section{Feature comparison}
\label{sec:feature-comparison}

This survey only includes hosted CI/CD services, referring to those which offer a \textit{Software as a Service} (\textit{SaaS}) model~\footnote{\url{https://azure.microsoft.com/en-us/overview/what-is-saas/}} of their application.
Services focused on one particular use case, such as Codacy~\footnote{\url{https://www.codacy.com/}} for code quality analysis or Snyk~\footnote{\url{https://snyk.io/}} for security, are excluded from this survey.
Similarly, the comparison excludes services which only support one particular platform or operating system.
Finally, CI/CD services from public cloud providers are omitted, since their implementation is tightly coupled with the rest of their ecosystem (e.g., AWS CodeStar).

Table \ref{tab:ci-cd-services} shows the list of  CI/CD services selected for this study, according to the criteria above.
The version numbers are indicated in the references.
If the service does not expose any version information, the access date is indicative.
The importance of the table columns is explained in the following sections.
The features selected for this study focus on quantitative aspects, since those can be objectively and fairly compared between different services.
%% Not part of our study are qualitative aspects, such as user friendliness, configuration complexity or setup effort.
Conversely, our study does not consider qualitative aspects, such as user friendliness, configuration complexity and setup effort.

\subsection{Source Repositories}
\label{subsec:source-repositories}

The software source code is the starting point of any CI/CD process.
Currently, the majority of developers choose Git as their version control system \cite{stack_exchange_inc_stack_2018}.
Therefore, many CI/CD services focus solely on support for Git.
%All services examined in this report exclusively support Git, except Bitbucket Pipelines, which also supports Mercurial (Table~\ref{tab:ci-cd-services}).
Notable exceptions in our study are AppVeyor and Bitbucket Pipelines, which both additionally support Mercurial (Table~\ref{tab:ci-cd-services}, \textit{Source Repositories}).

Often, developers find it useful to run different steps of the pipeline only for specific versions of the source code.
For example, deployments should only be done for the \texttt{master} or \texttt{production} branch.
To support this, CI/CD services allow defining build rules based on VCS-refs (\textit{branches} and \textit{tags}).
Additionally, few CI/CD services also have the capability to only execute a pipeline step when a particular set of files changed (Table~\ref{tab:ci-cd-services}, \textit{Build Rules}).
This is important for \textit{monorepos}, where code for separate applications coexists in the same repository, because it avoids running the entire pipeline for each change.
Instead, only the pipeline jobs relevant to the change may be executed.

\clearpage

\newgeometry{top=0.5cm,right=2cm}
\begin{sidewaystable}
\onehalfspacing
\centering
\caption{Feature Matrix of CI/CD services.}
\label{tab:ci-cd-services}
\begin{scriptsize}
%% \begin{sf}
\begin{tabularx}{0.97\paperheight}{@{}|p{2.4cm}||p{3cm}|p{1.3cm}|p{1.7cm}|L|L|p{1.4cm}|p{1.7cm}|p{2cm}|L|L|L|@{}}
  \hline
  \textbf{Service} & \textbf{Source Repositories} & \textbf{Artifacts} & \textbf{Build rules} & \textbf{Runner \mbox{self-hosting}} & \textbf{Coordinator \mbox{self-hosting}} & \textbf{Build OS} & \textbf{Isolation} & \textbf{Configuration} & \textbf{Triggers} & \textbf{Notifications} \\
  \hline \hline
  AppVeyor \cite{appveyor_systems_inc_appveyor_nodate} & GitHub, Bitbucket, GitLab, Azure Repos, Kiln, Gitea, any Git, Mercurial or Subversion repository & native & VCS-refs, files, commit author & yes (proprietary) \footnote{\url{https://www.appveyor.com/docs/byoc/}} & yes (proprietary) \footnote{\url{https://www.appveyor.com/self-hosted/}} & \faLinux~\faApple~\faWindows & VM & \texttt{appveyor.yml}, Web~UI & Push, PR, Schedule & (native) E-Mail, Slack, HipChat, CampFire \\
  \hline
  Bitbucket Pipelines \cite{atlassian_bitbucket_nodate} & Bitbucket Cloud (Git, Mercurial) \footnote{\url{https://confluence.atlassian.com/get-started-with-bitbucket/dvcs-workflows-for-bitbucket-860009652.html}} & native & VCS-refs & no & no & Unix & Docker & \texttt{.bitbucket- pipelines.yml} & Push, Schedule, Manual & (native) Slack, E-Mail; (extensible via \textit{Pipes}) \\
  \hline
  CircleCI \cite{circle_internet_services_inc_circleci_nodate} & GitHub, Bitbucket (Git, Mercurial) & native & VCS-refs & no & yes (paid) & \faLinux~\faApple~\faWindows & VM, Docker & \texttt{.circleci/ config.yaml} & Push, Schedule, API & (native) Slack, IRC, E-Mail; (extensible via \textit{Orbs}) \\
  \hline
  Cirrus CI \cite{cirrus_labs_cirrus_nodate} & GitHub & native & VCS-refs, files & Google Cloud, AWS, Azure, Anka & no & \faLinux~\faApple~\faWindows \newline FreeBSD & VM, Docker & \texttt{.cirrus.yml} & Push, Schedule, Manual & no \\
  \hline
  Concourse CI \cite{concourse_contributors_concourse_nodate} & Any Git or Mercurial repository; (extensible via \textit{Resources}) & external & none & yes (Apache 2.0) & yes (Apache 2.0) & \faLinux~\faApple~\faWindows & Docker & API (via~\texttt{fly}~CLI) \footnote{\url{https://concourse-ci.org/fly.html}} & (via Resources) Push, PR, Schedule, Manual, Datadog, Docker Image, RSS & (extensible via \textit{Resources}: Slack, IRC, HipChat, RocketChat) \\
  \hline
  Drone \cite{droneio_inc_drone_nodate} & GitLab, GitHub, Bitbucket Cloud, Gitea & external & VCS-refs & yes (Apache 2.0) & yes (Apache 2.0) & \faLinux~\faApple~\faWindows & Docker, \newline Kubernetes, Local ("Exec"), SSH & \texttt{.drone.yaml}, Starlark, JSONnet \footnote{\url{https://docs.drone.io/extensions/conversion/}} & Push, PR, Schedule & (extensible via \textit{Plugins}) \\
  \hline
  GitHub Actions \cite{github_inc_github_nodate} & GitHub & native & VCS-refs, files & yes (MIT) \footnote{\url{https://github.com/actions/runner}} & no & \faLinux~\faApple~\faWindows & VM, Docker & \texttt{.github/ \{workflows, actions\}/ *.yml} & Push, PR, Schedule, API, GitHub event & (extensible via \textit{Actions}) \\
  \hline
  GitLab CI/CD \cite{gitlab_inc_gitlab_nodate} & GitLab, GitHub, Bitbucket Cloud, any Git repository & native; container registry & VCS-refs, files & yes (MIT) & yes (MIT) & \faLinux~\faApple~\faWindows \newline BSD & SSH, Local ("Shell"), VM, Docker, Kubernetes & \texttt{.gitlab-ci.yml} & Push, PR, Schedule, API & (native) E-Mail, Discord, Hangouts, HipChat, Mattermost, Microsoft Teams, Slack, Unify Circuit \\
  \hline
  Travis CI \cite{travis_ci_gmbh_travis_nodate} & GitHub & external & VCS-refs & no & no & \faLinux~\faApple~\faWindows & VM & \texttt{.travis.yml} & Push, PR, Schedule, API & (native) E-mail, IRC, Campfire, Flowdock, Hipchat, Pushover, Slack, OpsGenie \\
  \hline
\end{tabularx}
%% \end{sf}
\end{scriptsize}
\end{sidewaystable}
\restoregeometry

%% add pricing scheme / metric ?

\clearpage

\subsection{Triggers}
\label{subsec:triggers}

The CI/CD service runs the pipeline for a repository when an event, referred to as \textit{trigger}, happens.
The most common trigger is a change in the source repository, called a \textit{push event}.
To keep track of modifications to the source code, one approach for the CI/CD system is polling the repository for changes.
This has the advantage of being completely independent of the VCS platform.
However, it does not scale with many repositories, and consequently this approach is no longer commonly used.
The more efficient approach is letting the source code platform push new events directly to the build server, which is implemented via \textit{webhooks}~\footnote{\url{https://codeburst.io/what-are-webhooks-b04ec2bf9ca2}} (HTTP requests).
The disadvantage of this approach is the lack of a common protocol for these notifications; therefore every event source -- e.g., GitHub or GitLab -- needs to be implemented by each build service.
In our study, Concourse CI is the only service that polls event sources by default, though primitive support for webhooks is available \cite{concourse_contributors_concourse_nodate}.

Other triggers may include events unrelated to changes in the source code, such as \textit{cron schedules} (regular timers) or manual pipeline triggers via a web interface or API (Table~\ref{tab:ci-cd-services}, \textit{Triggers}).

\subsection{Configuration}
\label{subsec:configuration}
Most CI/CD services use a \textit{Pipeline as code} approach~\footnote{\url{https://jenkins.io/solutions/pipeline/}} for the build pipeline configuration, which means the pipeline configuration is stored alongside the regular application source code in each repository.
This approach has the benefit that the configuration can be code reviewed, it has an audit trail via the VCS history and it is stored in a central location where it can be edited by any team member.
The configuration file specifies the programming language and desired build environment, among other parameters.
A versioned and peer-reviewed pipeline configuration is especially important for deployment steps, since these are mission-critical as they interact with production infrastructure.
Concourse CI is the only service in our study not following this approach (Table~\ref{tab:ci-cd-services}, \textit{Configuration}).
Instead, the configuration file is uploaded and applied with a command line client, called \texttt{fly} \cite{concourse_contributors_concourse_nodate}.
While the configuration file can optionally be stored in a repository, it is not required and does not affect the pipeline configuration.

The majority of build services support a YAML configuration exclusively.
A notable exception is Drone which supports Starlark and JSONnet (Table~\ref{tab:ci-cd-services}, \textit{Configuration}).
Although YAML is a simple and human-readable configuration format, it lacks enforced structure and depends on indentation for hierarchy.\footnote{\url{https://noyaml.com/}}
Even though most services use YAML for their configuration (file extension \texttt{.yml} or \texttt{.yaml}), the internal structure of the file differs from tool to tool.
As a result, manual intervention is required when migrating between services, even if two services offer the same features.
A sample YAML configuration is presented in Appendix \ref{sec:example-yaml-config}.

\subsection{Artifacts}
\label{subsec:artifacts}
Intermediate or final results of the pipeline are called \textit{artifacts}.
Examples for these are binaries, documentation or generated archives.
Artifacts are persisted after a pipeline has finished and may be used for longer-term storage of the outputs.
A common use case is making test and build results available to the user, preferably via an integrated web UI or API.

Some CI/CD services, such as Drone and Concourse (Table~\ref{tab:ci-cd-services}, \textit{Artifacts}), choose to not implement persistent artifacts storage~\footnote{\url{https://github.com/drone/drone/issues/966\#issuecomment-91316397}} due to the added maintenance burden and software complexity.
Instead, the pipeline needs to implement another step for uploading these artifacts to an external storage service, as shown in Appendix~\ref{sec:example-yaml-config}.

Native artifact support dramatically eases the pipeline setup for developers, since no additional infrastructure needs to be provisioned and the pipeline configuration is simpler.
Some services, for example GitLab~\footnote{\url{https://about.gitlab.com/stages-devops-lifecycle/package/}}, not only offer storage of arbitrary files, but even implement repository endpoints, such as container or package registries.

\subsection{Build Isolation}
\label{subsec:build-isolation}

CI/CD services commonly split their architecture into two distinct parts.
The \textit{coordinator}, also referred to as \textit{build master}, handles the interaction with repositories and provides a web interface.
The \textit{runner}, also called \textit{build agent}, is a separate application, running on a different machine, which executes the steps specified by the coordinator.
Many agents can be connected to a single coordinator, which allows the architecture to scale seamlessly and provides a separation of concerns.
Additionally, this design enables running pipeline steps in different build environments.

%% * Container
%% * VMs
%% * Native (different architectures)
%% * SSH

Virtualization and containerization have revolutionized the way CI/CD systems provision the build environment. % set up, implement, ...
Most importantly, they provide reproducibility and isolation between different pipeline runs.
Furthermore, these concepts make the build capacity scalable.
New agents can be quickly provisioned due to the abstraction and programmatic setup of the build environment, instead of having to run manual setup steps for each system.
In this regard, containers have a smaller size than virtual machines (VMs), thus making them faster to deploy.
They also come with clear build instructions (e.g., Dockerfile), resulting in a consistent output \cite{sharma_containers_2016}.
Due to these properties, containers are well-suited for archival for later usage or inspection.
Additionally, containers can be shared conveniently through online services, called \textit{registries}.\footnote{\url{https://www.docker.com/products/docker-hub}}

However, containers are not suitable for every workload.
VMs provide greater levels of isolation from the host operating system \cite{sharma_containers_2016}.
As a result, VMs are a better choice if configuration of the underlying operating system (i.e. kernel) is required to perform pipeline steps.
Likewise, virtual machines allow using any operating system (e.g., Linux, macOS, Windows, BSD) for a pipeline (Table~\ref{tab:ci-cd-services}, \textit{Build OS}).

Sometimes local (or ``native'') execution environments are desirable, such as for the performance assessment of an application.
In this case, no other software should run on the agent, to ensure the tests are not affected by any environmental side-effects.
For similar scenarios, executing an entire build pipeline via a remote shell (SSH) is a useful functionality.
Examples of this include leveraging hardware features of a specific machine without having to install the pipeline execution software.

The programming languages supported by a CI/CD service used to be an important feature, but due to the ubiquity of virtualization described above, this is no longer a differentiating factor.
By using different VMs or container images, build services have become language-agnostic.
Hence, this parameter has been omitted from the feature comparison.

\subsection{Self-hosting}
\label{subsec:self-hosting}

This study focuses on managed CI/CD services, nevertheless two of the evaluated tools -- Gitlab and Drone -- allow users to install the applications on their own hardware, due to the permissive licensing.
Some organizations favor this approach due to concerns about the security and stability of publicly available services having access to their source code.

As explained in Section \ref{subsec:build-isolation}, the coordinator and agent are two distinct applications.
For this reason, most CI/CD services allow users to set up their own agent instances (Table \ref{tab:ci-cd-services}, \textit{Self-hosting}).
The reasons for doing so include faster pipeline execution, utilization of private or custom hardware and isolation of deployment environments.

\subsection{Notifications}
\label{subsec:notifications}

The final step in a CI/CD pipeline is notifying the developers about the status.
By convention, the status is derived off of the scripts' exit code.
%If it returns 0, then the task was successful, otherwise it failed.
Especially in the event of a failed task, it is important to notify the original authors of the code change, thus giving them the possibility to promptly examine the failure.

Moreover, it might be necessary or desirable to publish information about successful events nevertheless, for instance, when a new version of the application was deployed to a production environment.
For these notifications, instant messaging platforms -- such as Slack or Mattermost -- are commonly used in many organizations.
%In addition, it might also be desirable to connect issue trackers or project management software to the pipeline.

On the one hand, CI/CD services with native integrations to these platforms drastically reduce the setup and configuration overhead compared to maintaining these integrations separately in the build pipeline.
For example, GitLab offers native integrations for 30 different services~\footnote{\url{https://docs.gitlab.com/12.7/ee/user/project/integrations/project\_services.html\#services}}, which can easily be enabled and configured through a web interface.

On the other hand, avoiding manual configuration is exactly the point of the \textit{Infrastructure-as-Code} paradigm \cite{huttermann_infrastructure_2012}.
Other services do not have native integrations, but rather offer a set of pre-configured Docker images.
These only need to be provided with appropriate credentials through environment variables.
Each of the services refers to these extensions in a different way (Table~\ref{tab:ci-cd-services}, \textit{Notifications}).
This approach minimizes the effort for creating new integrations, especially for third-party developers, thereby naturally enabling a wider range of integrations.
However, third-party extensions, just like software dependencies, need to be treated with care, since they can contain software bugs and security vulnerabilities, like any other software.
This is especially relevant in the context of CI/CD, since the deployment steps have access to security credentials.

\clearpage

\section{Conclusion}
\label{sec:conclusion}

This paper introduced the notion of continuous integration (CI), continuous delivery (CDE) and continuous deployment (CD).
With these continuous practices in mind, we examined the feature sets of nine managed, general-purpose CI/CD systems: AppVeyor, Bitbucket Pipelines, CircleCI, Cirrus CI, Concourse CI, Drone, GitHub Actions, GitLab CI/CD and Travis CI.
Overall, we found that the basic features of most CI/CD services are similar, however there exist major differences in their implementation, e.g., in terms of integrated features versus extension-based features.
We also provided specific suggestions on when to use certain features, such as containers versus virtual machines.

While this study has focused on quantitative aspects of selected services, also the usability of interfaces needs to be considered in human-computer interactions (HCIs) \cite{jacko_human_2012}.
In the context of CI/CD services, the main interfaces between human and software are the web UI and configuration of the CI/CD service.
This aspect should be evaluated in a future study.

Another qualitative aspect of CI/CD services, which has not been taken into account in this study, is how strictly the recommended workflow of a service needs to be follow, i.e., how \textit{opinionated} they are.
Some services are very flexible and can facilitate any workflow a team might already have established.
This comes at the cost of increased complexity and configuration overhead.
Opinionated services provide a strict framework that dictates how they should or can be used.
It works well when the desired workflow matches the recommended one closely, or there has not been any workflow before, but can become challenging when solving a problem that can not be mapped onto the provided tools.
Ultimately, this distinction has major implications on how the CI/CD process works and how the developers interact with it.

Furthermore, due to our service selection criteria, entirely self-hosted solutions have been omitted from this study.
Examples for such applications include Jenkins, GoCD and Bamboo.
Generally speaking, these tend to be more flexible and powerful compared to hosted alternatives.
Therefore, they are more widely used in large enterprises, which also have the resources to maintain them.
The feature sets of these applications should be compared in a similar study.

%============================================================
\clearpage

% sloppy allows TeX to expand the amount of interword whitespace (almost) arbitrarily to support its efforts to avoid overfull lines and URL hyphenation issues
\begin{sloppypar}
\bibliographystyle{IEEEtranUrldate}
\bibliography{cs-seminar}
\end{sloppypar}

%============================================================

\clearpage

\appendix
% set section numbering in appendix to letters
\renewcommand{\thesection}{\Alph{section}}
\section{Example YAML pipeline configuration}
\label{sec:example-yaml-config}

The YAML configuration file in Listing \ref{src:drone-yaml-example} shows the pipeline used for rendering and uploading this paper~\footnote{\url{https://drone.cubieserver.de/jh/cs-e4000-seminar}} with Drone.
The download of the source repository is implicit, Drone performs it automatically.
The first pipeline step (line 5-10) generates the PDFs by executing the \texttt{latexmk} command inside a Docker container based on the specified \texttt{image}.
The second step (line 11-26) is only executed if the first one succeeds.
If this pipeline runs on the \texttt{master} branch (line 22-24), it uploads the generated PDFs (\texttt{source} attribute) to an externally provisioned storage server (\texttt{endpoint} attribute), thereby making them publicly viewable. % accessable

\lstinputlisting[
  language=yaml,
  numbers=left,
  caption=Example Pipeline Configuration for Drone (.drone.yml).,
  label=src:drone-yaml-example
]{.drone.yml}

\end{document}
