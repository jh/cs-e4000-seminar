\documentclass[aspectratio=169,handout,12pt]{beamer}
% use documentclass[handout]{beamer} to collapse each frame into one page
\usetheme{focus}

\usepackage[scale=2.5]{ccicons}
\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{--}

\title{A comparison study of \\managed CI/CD solutions}
%% \subtitle{Subtitle}
\author{Jack Henschel}
\titlegraphic{\includegraphics[scale=0.175]{../figures/devops-loop}}
\institute{Aalto University \\Seminar in Computer Science}
\date{\today}

\begin{document}
    \begin{frame}
        \maketitle
    \end{frame}

    % Use starred version (e.g. \section*{Section name})
    % to disable (sub)section page.

    %=============================================================================%

    \begin{frame}{Overview}
      \begin{itemize}
      \item Background
        \begin{itemize}
        \item Software engineering challenges
        \item Continuous practices
        \end{itemize}
      \item Feature comparison of CI/CD systems
        \begin{itemize}
        \item Source Repositories \& VCS
        \item Configuration
        \item Artifacts
        \item Build OS \& Isolation
        \item Architecture \& Self-hosting
        \end{itemize}
      \item Conclusion
      \end{itemize}
    \end{frame}

    %=============================================================================%
    \section{Background}
    \begin{frame}{Software engineering in 2020}
      %% These are challenges in software development, they are not problems}
      \begin{itemize}
        \pause

      \item Speed of software and hardware development cycles
        %% Example: product cycle of smartphones

        \pause

      \item Complexity of software internally
        %% Applications are becoming more complex because they contain more and more features, example: web browsers

        \pause

      \item Complexity of integrated services
        %% Applications are connected to many external services via APIs

        \pause

      \item Geographically distributed development teams (Remote working)
        %% The software engineering field has pioneered this field, now everyone is / has to do it

      \end{itemize}

    \end{frame}

    %=============================================================================%

    %% In order to cope with these challenges, new software engineering practices have emerged
    \begin{frame}{How can developers stay productive?}

      \pause

      \begin{itemize}
        %% Continuous practices are widely used in software development, from hobby projects to small startups and large enterprises.
      \item Continuous practices are used in hobby projects, small startups and large enterprises

        \pause

      \item Rooted in agile software development methodologies \cite{matthies_attitudes_2019}

        \pause

      \item Three stages:
        \begin{itemize}
        \item \textbf{CI}: continuous integration
        \item \textbf{CDE}: continuous delivery
        \item \textbf{CD}: continuous deployment
        \end{itemize}

      \end{itemize}

    \end{frame}

    %=============================================================================%

    \begin{frame}{Continuous practices}

      %% In practice, continuous workflows usually look like this...

      \begin{center}
        \includegraphics[width=\textwidth]{../figures/ci-cd-workflow.png}
      \end{center}

      \pause

      \begin{enumerate}
        %% Continuous integration is based on the maxim of every team member contributing to the main development branch (usually referred to as ``master'') of the project
      \item Every team member contributes to the main branch of the software project (\textbf{continuous integration})
        %% this means there should be no long running feature branches
        %% rather, small and incremental changes should be merged back into the main branch of the project

        \pause

        % every change (called "commit") to the source code is automatically evaluated using a test suite
      \item \emph{Automatically} test every change (\textit{commit}) to the software

        \pause

        % this helps to always keep the software in a release-able state
        % so that at any point in time it can be shipped to customers
      \item Keep the software in a release-able state (\textbf{continuous delivery})

        \pause

      \item Software is automatically rolled out to target devices (\textbf{continuous deployment})
        %% of course only if the organization developing the code also has access to the servers running the code
      \end{enumerate}

      \pause

      $\Rightarrow$ Facilitated by a chain of tools
      %% These stages of continuous practices are supported by a chain of tools
      %% Tools help to manage the risks by providing insights into code changes, automating common tasks to make them repeatable and error free, and logging every action of the process
    \end{frame}

    %=============================================================================%
    \section{Feature comparison}
    %% To understand how to build an efficient and successful CI/CD workflow
    %% we will look into the most important features of a pipeline

    \begin{frame}{Source Repositories \& VCS}
      %% Every CI/CD pipeline starts at the source code repository

      \pause

      \begin{itemize}

      \item Git is most popular VCS \cite{stack_exchange_inc_stack_2018}

        (GitHub, GitLab, Gitea, ...)
        %% According to the stack overflow developer survey,
        %% Git is choosen by X percent of developers.
        %% Popular options for hosting source code are Github, Gitlab, Gitea, Bitbucket Pipelines

        $\rightarrow$ many CI/CD services only support Git
        %% however, there are some exceptions to this, which also support Mercurial or even Subversion (AppVeyor)

        \pause

        %% after the developers have committed and pushed their changes online,
        %% the source code platform notifies the CI/CD service of this change.
        %% this is called an event and is commonly implemented with webhooks
      \item Source code platform notifies CI/CD service about changes

        $\rightarrow$ \emph{triggers} pipeline execution

        %% Other events that trigger a pipeline execution may be unrelated to changes
        %% in the source code, such as regular timers with cron schedules
        %% or manual pipeline executions via a web interface or API

        \pause

        %% Often, developers find it useful to run different steps of the pipeline
        %% only for specific versions of the source code. For example, deployments
        %% should only be done for the master or production branch.

      \item Build rules are used to run different pipeline steps depending on:

        \begin{itemize}
          %% To support this, CI/CD services allow defining build rules
          %% based on VCS-refs (branches and tags).
        \item VCS-refs (\emph{branches}, \emph{tags})
          %% Additionally, few CI/CD services also have the capability to only
          %% execute a pipeline step when a particular set of files changed
          %% This is important for monorepos, where code for separate
          %% applications coexists in the same repository, because it avoids running
          %% the entire pipeline for each change. Instead, only the parts relevant to
          %% the change may be executed.
        \item Changed files
        \end{itemize}

      \end{itemize}

    \end{frame}

    %=============================================================================%

    %% this brings us to our next topic: configuration of the build pipeline
    \begin{frame}{Configuration}

      \pause

      \begin{columns}
        \column{0.6\textwidth}
        %% Most CI/CD services use a Pipeline as code approach for the build pipeline
        %% configuration, which means the pipeline configuration is stored alongside
        %% the regular application source code in each repository.
        \emph{Pipeline as code}: configuration file is stored in the source repository
        %% This approach has the benefit that it can be code reviewed, it has an
        %% audit trail via the VCS history and it is stored in a central location
        %% where it can be edited by any team member.

        \vspace{1cm}
        \pause

        One exception: Concourse CI $\rightarrow$ \texttt{fly} CLI \cite{concourse_contributors_concourse_nodate}

        %% Concourse CI is the only service in the study that does not
        %% follow this approach. Instead, the configuration file is uploaded
        %% and applied with a command line client, called fly
        %% While the configuration file can optionally be stored in a repository,
        %% it is not required and does not affect the pipeline configuration.

        \vspace{1cm}
        \pause

        Most widely used configuration format: YAML

        %% YAML is a simple and human-readable format, it lacks enforced structure
        %% and depends on indentation for hierarchy.
        %% Even though most services use YAML for their configuration,
        %% the internal structure of the file differs from tool to tool.
        %% As a result, manual intervention is required when
        %% migrating between services, even if two services offer the same features.

        \pause

        \column{0.4\textwidth}

        %% Here, we have an example of a YAML configuration file
        %% In fact, this is the pipeline used for rendering and uploading this paper with Drone.
        %% The download of the source repository is implicit, Drone performs it automatically.
        %% The first pipeline step (line 5-10) generates the PDFs by executing the \texttt{latexmk} command inside a Docker container based on the specified \texttt{image}.
        %% The second step (line 11-26) is only executed if the first one succeeds.
        %% If this pipeline runs on the \texttt{master} branch (line 22-24), it uploads the generated PDFs (\texttt{source} attribute) to an externally provisioned storage server (\texttt{endpoint} attribute), thereby making them publicly viewable.

        \includegraphics[width=\textwidth]{../figures/example-yaml}

      \end{columns}

    \end{frame}

    %=============================================================================%

    \begin{frame}{Artifacts}

      \begin{columns}
        \column{0.6\textwidth}

        %% Intermediate or final results of the pipeline, such as binaries, documentation
        %% or generated archives, are called artifacts.

        Artifacts: \textit{Stored results of a pipeline}

        (binaries, documentation, archives, ...)

        % as we could see before, a large chunk of the pipeline defintion is just setting
        % up the archival of the PDFs

        \pause
        \vspace{1cm}

        Native vs. external artifact storage

        %% Native artifact support dramatically eases the pipeline setup for developers,
        %% since no additional infrastructure needs to be provisioned and the pipeline
        %% configuration is simpler.


        %% Some CI/CD services -- such as Drone and Concourse --
        %% choose to not implement persistent artifacts storage due to the added
        %% maintenance burden and software complexity.
        %% In that case, the developers need to provision external storage resources
        %% themselves and upload their artifacts there,
        %% as was shown in the previous slide

        \pause
        \vspace{1cm}

        %% Here we have an example of artifacts with Gitlab CI

        \begin{center}
          \includegraphics[width=0.4\textwidth]{../figures/artifact-config-gitlab.png}
        \end{center}

        \pause

        \column{0.4\textwidth}

        \includegraphics[width=\textwidth]{../figures/artifacts-ui-gitlab.png}

      \end{columns}

    \end{frame}

    %=============================================================================%

    \begin{frame}{Build OS \& Isolation}

      \begin{columns}

        \column{0.5\textwidth}

        \includegraphics[width=\textwidth]{../figures/vms-containers.png}

        \column{0.5\textwidth}

        \begin{itemize}

          \pause

          %% Virtualization and containerization have revolutionized the way CI/CD
          %% systems provision the build environment.

        \item Virtualization and Containerization of all pipeline workloads

          %% Containers have a smaller size than virtual machines (VMs), thus making them faster to deploy. They also come with
          %% clear build instructions (e.g., Dockerfile), resulting in a consistent output
          %% Additionally, containers can easily be shared through so-called *registries*.

          %% However, containers are not suitable for every workload. VMs provide
          %% greater levels of isolation from the host operating system.
          %% As a result, VMs are a better choice if configuration of the underlying operating
          %% system (i.e. kernel) is required to perform pipeline steps.
          %% Likewise, virtual machines allow using any operating system
          %% for the pipeline (Linux, Windows, macOS, BSD).

          \pause

        \item Clean build environment $\rightarrow$~\emph{reproducibility} and \emph{isolation}

          \pause

        \item Elastic build capacity

          %% New agents can be quickly provisioned due to the abstraction and programmatic setup of
          %% the build environment, instead of having to run manual setup steps for each of them.

        \end{itemize}

      \end{columns}

    \end{frame}

    %=============================================================================%

    %% This elastic build capacity is also faciliated by the common architecture all CI/CD services share

    \begin{frame}{Architecture \& Self-hosting}

      \begin{center}
        \includegraphics[width=0.6\textwidth]{../figures/coordinator-agent-architecture.png}
      \end{center}

      \begin{itemize}

        \pause

        %% The \textit{coordinator} handles the interaction with repositories and provides a web interface.
        %% The \textit{build agent} is a separate application, running on a different machine, which executes the steps specified by the coordinator.
        %% Many agents can connect to a single coordinator, which allows the architecture to scale seamlessly.

      \item Platform architecture split into two: \emph{coordinator} and \emph{agent}

        \pause

        %% Furthermore, this design allows "outsourcing" the agents

      \item Managed vs. self-hosted agents

        %% By default, all services evaluated in our study offer managed agents, which are billed by the amount of time they are used.
        %% Additionally, most CI/CD services allow users to set up their own agent instances.
        %% The reasons for doing so include faster pipeline execution, utilization of private or custom hardware and isolation of deployment environments.
        %% One example is getting access to servers that are deployed behind a firewall.
        %% Another example is using specific hardware that you have installed on-premise and would like to use to test your application against.

        \pause

      \item Self-hosting of entire platform

        %% This study focuses on managed CI/CD services, nevertheless two of the evaluated tools, namely GitLab and Drone,
        %% allow users to install the applications on their own hardware, due to the permissive licensing.
        %% Some organizations favor this approach due to concerns about the security and stability of
        %% publicly available services having access to their source code.

      \end{itemize}

    \end{frame}

    %=============================================================================%

    \section{Conclusion}
    \begin{frame}{Conclusions}
      \begin{itemize}

      \item Continuous practices greatly enhance software development
        %% Once the initial investment into setup of the infrastructure has been made, it continuously keeps paying dividends

        \pause

       %% Travis-CI launched in 2011, and over ...
      \item CI/CD services have become mainstream \cite{chen_continuous_2015} \cite{hilton_usage_2016}
        %% not only have they become mainstream, but also easy to use
        %% with a few clicks and a simple configuration file, anyone can set up a basic pipeline

        \pause

      \item Configuration converged on YAML files in the source repository
        %% This is mostly a positive development, however the pipeline declarations can get very big and un-maintainable

        \pause

      \item Most CI/CD services offer same set of basic features
        \newline
        $\rightarrow$ basic use-cases always covered

        \pause
        %% when choosing a CI/CD service, special attention needs to be paid to the required build environment
        %% for example when you want to build macOS applications with Xcode
        %% or when you need to set up a special server for execution of local commands
        %% the same applies to the set of available integrations, triggers and notifications
        %% for example, if you're using IRC or another less mainstream chat protocol, then you should look
        %% into one of the build services that supports plugins
      \item Attention to:
        \begin{itemize}
          \item specific build environments (OS, isolation)
          \item integrations (VCS, triggers, notifications)
        \end{itemize}
      \end{itemize}

    \end{frame}

    %=============================================================================%

    \begin{frame}{Thanks for your attention!}

      %% With that, I would like to thank you for your attention and I welcome any question on your behalf.

      \pause

      \begin{center}
        \large{\textit{\textbf{``\inserttitle''}}}

        -- \insertauthor\ (\insertdate)
      \end{center}

      \vspace{1em}

      Source Material available at:
      \begin{center}
        \url{https://git.cubieserver.de/jh/cs-e4000-seminar}
      \end{center}

      Licensed under \href{http://creativecommons.org/licenses/by/4.0/}{Creative Commons
        Attribution 4.0 International License}
      \begin{center}
        \ccby
      \end{center}

    \end{frame}

    %=============================================================================%

    \appendix
    \begin{frame}[allowframebreaks]{References}
        %% \nocite{*}
        \bibliography{../cs-seminar}
        \bibliographystyle{plain}
    \end{frame}

\end{document}
